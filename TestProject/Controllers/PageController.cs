﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestProject.Controllers
{
    public class PageController : Controller
    {
        // GET: Page
        public ActionResult SignIn()
        {
            var path = Path.Combine("~", "Views", "CommonPages", "SignIn.cshtml");
            return View(path);
        }
    }
}