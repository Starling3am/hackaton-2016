﻿'use strict';

angular
    .module("testproject", [
        'ngRoute',
        'testproject.controllers'
    ])
    .config(function ($routeProvider, $locationProvider) {
        $routeProvider
            .when('/signin', { templateUrl: 'page/signin', controller: 'signInController' })
            .otherwise({ redirectTo: '/signin' });
    })
    .run(function () {

    });