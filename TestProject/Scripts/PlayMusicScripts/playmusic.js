﻿var Player = function () {
    this.constructor = function () {

    }

    this.constructor();
};

Player.prototype.playMusic = function (elementId, url) {
    var self = this;
    var audioElement = document.getElementById(elementId);
    self.setSourceUrlToAttribute(audioElement, url);
    self.PlayStopMusic(audioElement);  
}

Player.prototype.setSourceUrlToAttribute = function (audioElement, url) {
    if (!this.isSourceSetuped(audioElement)) {
        audioElement.setAttribute("src", url)
    }
}

Player.prototype.isSourceSetuped = function (audioElement) {
    var attr = audioElement.getAttribute("src");
    return attr == "#" ? false : true;
}

Player.prototype.PlayStopMusic = function (audioElement) {
    var self = this;
    if (self.isPlaying(audioElement)) {
        audioElement.pause();
    } else {
        audioElement.play();
    }
}

Player.prototype.isPlaying = function (auElement) {
    if (auElement.paused == null)
        return false;
    return !auElement.paused;
}

var PlayMusic = new Player();